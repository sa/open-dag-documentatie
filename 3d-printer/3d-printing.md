# 3d printing

## Preparing the printer

  - Check tape on print bed, replace if needed. Use only the blue tape
  - Apply pritt (use the stift) to the print bed
  - Check filament. Is it still enough?
  - Let it preheat for half an hour. Extruder should be preheated at 220, print
      bed at 65. 

## Preparing the gcode

  - Follow the steps on https://builder3dprinters.com/software/
    - Use Cura 3.2
    - Apply settings changes given on the website
    - Apply settings changes on the images
  - Add models to cura and rotate/relocate for optimal printing
  - Load gcode to SD

## Printing

  - Load SD card
  - Auto-home
  - Start print
  - Check frequently, things might break
