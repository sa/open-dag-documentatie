---
header-includes:
    - \usepackage{graphicx}
---
# HTC Vive demo setup

This setup uses two HTC vive's in a single playing field. 

## Requirements and credentials

Numbers are (for two vives / for one vive)

  - NDL PC 1
  - NDL PC 2 (x1 / x0)
  - Peripherals and cables for the pc's
  - HTC Vive box (2x / x1)
  - Tripods (x2)
  - Large table (2x)
  - Power sockets (4x on each side, not including controller chargers)
  - Red/white or yellow/black warning tape to mark the floor
  - Duct tape / paper tape

### Credentials:

  - Steam (NDL PC 1)
    - user: opendagru1
    - pass: opendagru
  - Steam (NDL PC 2)
    - user: opendagru2
    - pass: opendagru

## The day before the setup

Make sure:

  - All vive controllers are charged
  - The PC's are logged into the correct steam account (credentials above)
  - Steam VR is installed correctly
  - The Lab (the steam game we use) is installed correctly
  - You have two HTC Vive boxes without a cracked screen available. You may need
      to unscrew them from the ceiling in the New Devices Lab

## Example setup

\includegraphics[width=\textwidth]{vive-example-setup}

  - It is possible to place the setup in front of a door, as long as:
    - The door is not actively in use. It will **always** need to be available
      as an emergency exit
    - The tables / tripods are NOT in front of a door
    - All cables in front of the door are taped firmly to the ground

  1. HTC motion box. Put it on a tripod. Aim it on a 45 degree angle in the
     corner, about 30 degrees down. 
  2. Tables. If there is enough space, put the edges of the tables on the edge
     of the playing field, to prevent people from walking trough it. If this
     cannot be done because of space or fire safety reasons, put it further back
     to the wall. 
  3. The PC's powering the vive. To gain a bit of extra height for the screen,
     put them on top of the _lying down_ pc's. This extra hight is nice to have,
     because the people watching the screens will be standing. 
  4. The playing field. The total playing field can be a maximum of 5x3 meters.
     If you make it smaller, keep in mind that each vive can get only half of
     the playing field. The tiles in the Huygens building are 40x40 cm. Mark the
     playing field, as well as the edge of the playing field with the warning
     tape for safety reasons. 

  - Attatch the long power wires in the HTC Vive box to the Vive Motion Boxes. 
  - Put one motion box in mode A and the other in mode B.
  - Connect the motion boxes with the long jack cable. 
  - Connect the PC's, but do not yet turn them on. 
  - Take from the vive box:
    - The short power wire
    - The HDMI cable
    - The USB cable
    - A small box with HDMI, USB and power inputs
  - Connect the small box with the cables to the PC and the power socket, on the
      _non-orange_ side
  - Connect the HTC Vive to the orange side of the box
  - Make sure to tape the box firmly to the table

## Software setup

  - Power on the PC and start steam
  - Login to steam. If you did the preparations correctly, you can start steam
      in offline mode. 
  - Startup Steam VR in the upper right corner of the steam screen. 
  - Start the game _Room Setup_. Follow the instructions on screen. Make sure to
      choose _Advanced mode_ when measuring out the playing field. Keep ~20 cm
      (half a tile in the Huygens building) distance from the actual sides of
      the playing field. 
  - Start the game _The Lab_. 

## Letting people play the game

  - Make sure you start the game for them, because letting people do that
      themselves takes a long time. 
    - You can start the game in the _menu playing area_ by grabbing it and
        moving it to your head. Try not to hit the headset with the controller.
        We usually let people play the arrow shooting game. Grab the orb with
        the button on the back of the controller (the trigger)
    - You can move around the room by walking, but sometimes you meet the edge
        of the playing field. You can move around further by pressing the large
        touch-screen area of the controller and aiming where you want to go. 
    - In the new room, you can move around the same way. Move the controller
        _inside_ the hourglass icon somewhere on the table and press the
        trigger. 
  - If someone is wearing glasses, make sure to have them take them of. Wearing
      the Vive over glasses will damage both the glasses and the Vive. 
  - Show them the controller, and explain they only have to use the button on
      the back of the controller. Also explain that they first have to put on
      the headset, and you will give them the controller right after that. 
  - After you give them the controllers, make sure to tighten the straps around
      their wrists. 

### Rules you can explain immediately:

  - They can walk around freely, until they see a blue grid. 
  - They have to pick up the bow. Not point and grab a bit, but really pick it
      up. 
  - Shooting works the same way. You have to put the arrow on the bow, push the
      button to grab the string, tighten it, and release the button to release
      the arrow. 
  - They will need to defend the gate on the left side of the field. 
  - Shoot the person with the white flag to begin. 

### Extra rules to explain while playing:

  - They can set fire to an arrow to burn attackers' armor. 
  - They can shoot at the barrels / targets to release extra traps / explosions
  - They can shoot the balloons to regain some gate health
