# Handleiding Pannenkoekenprinter 2.0
Niek Janssen: **2018-09-27**

Update: Frank Gerlings **2019-01-30**

Translation to English: Denise Vonck **2019-03-08**

## Requirements
### Standard items
  - Printer
  - Hot plate holder
  - Hot plate
  - Router
  - 2 LAN-cables
  - Multiple socket(s) (with at least 6 sockets in total)
  - Electricity wires for the printer
  - 2 bowls or 1 shaker and 1 bowl
  - Very fine sieve
  - Whisk
  - Spatula
  - Kitchen scale
  - Measuring beaker
### To buy
  - Pancake mix "Koopmans Pannenkoekenbeslag Compleet", only water needed
  (1 pak per 2-3 hours of demonstration)
  - Napkins to serve the pancakes on
  - Powdered sugar/syrup ("stroop")
  - Kitchen paper 
  - Oil/butter
### Additional required items, bring yourself
  - Laptop with SSH
  - Waste bin
  - Cleaning supplies


## Preparing the printer
  - Put the printer somewhere with enough space
  - Put the hot plate in front of the printer. It can be difficult 
    to put the hot plate in the exact position, but it should fit exactly 
    and it is important that it is positioned correctly
  - Connect the printer and router with a LAN-cable. The laptop can be
    connected to the router with a LAN-cable as well, but connecting it to
    the wifi network will also work (SSID: NETGEAR63, password: ancientpond182)
  - Make sure that all components are powered:
    - The pi
    - The pi motor hat (which is on the pi)
    - The relay (via the powerbrick which is attached to it)
    - The hot plate
    - The router
    - The laptop that is used to run the program
  - Make an SSH connection with the Raspberry Pi
    - It is usually at port `192.168.1.2:22`. If this is not the case, then connect via your browser with the router at `http://192.168.1.1` and sign in with(user: user, password: password)
    - Log in (using PuTTY) to the pi with (user: pi, password: raspberry)
  - Make sure you read the hygiene instructions below before printing, especially on open days
  - Make the pancake batter as instructed below and put the tube in the bowl with batter

## Hygiene instructions
  - Make sure that the tube is cleaned on the inside and on the outside before printing. Cleaning the tube on the inside is done by pumping warm water through the tube for 5 minutes. Unscrew the clip completely for this. This can be done using the TUI (textual user interface), as explained later on
  - Rinse the tube after printing first with warm water WITH dreft (dish soap brand) for 10 minutes, then with warm water WITHOUT dreft for 10 minutes. Also clean the outside of the tube

## The batter
  - Use "Koopmans Pannenkoekenmix Compleet", to which you only need to add water. Other pancake mixes might require adding milk which can cause viscosity problems 
  - Mix 400 grams of pancake mix with 750 milliliter water. Use the kitchen scale and the measuring beaker to approximately get these proportions. Mix the batter well with a whisk until (almost) all chunks are dissolved. Do not do this step in the bowl in which the final printing batter should be!  
  - Pour the batter through the sieve into the bowl out of which you are going to print 
  - Grease the hot plate by rubbing it in with oil or butter using a paper towel

## Printing
  - Go to the folder `/home/pi/Desktop/Pannenkoekenprinter/`. `runme.py` runs the 
  printer. It is important to be in that folder, otherwise you will not find any images. Easier: you can execute the command `pancake` in any directory. Both methods will open a TUI in which you can choose various operations. Every operation can prematurely be stopped with `ctrl + c`. The printer will return to its original position after every operation
  The operations are:
    - Drawing a maximal range. This can be used to check whether the hot plate is positioned correctly
    - Flushing the tube. The standard time interval is 10 minutes, but this can be interrupted with `ctrl + c`
    - Showing a list of all printable images
    - Printing an image, given its name

## Vectorfiles
In the folder ``./examples`` there are already a few vector files. You can add new vector files to this yourself. The first line gives the x- and y-resolutions, separated with a comma. It is recommended to choose around 10 pixels for this, because of the thickness of the printing lines. Every next line defines a vector, a continuous line of batter. The format for such a vector is ``x1, y1 - x2, y2 - x3, y3``. Lines starting with a '#' are used as comments.